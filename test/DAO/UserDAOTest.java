/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.KhachHang;
import Model.NhanVien;
import Model.Role;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lamvi
 */
public class UserDAOTest {
    static UserDAO userDAO;
    public UserDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        userDAO = UserDAO.instance();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckLogin() {
        NhanVien nv = new NhanVien();
        nv.setUser_name("admin");
        nv.setPassword("admin");
        nv = userDAO.checkLogin(nv);
        assertEquals(nv.getMa(), "admin1");
    }
    
    @Test
    public void testGetRole() {
        Role role = userDAO.getRole(1);
        assertEquals(role.getMa(), "admin");
    }
    
    @Test
    public void testFindKhacHang() {
        KhachHang kh = userDAO.findKhacHang(13);
        assertEquals(kh.getMa(), "testjunit");
    }
}
