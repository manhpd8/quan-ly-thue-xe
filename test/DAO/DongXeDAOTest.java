/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.DongXe;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lamvi
 */
public class DongXeDAOTest {
    static DongXeDAO dongXeDAO;
    public DongXeDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        dongXeDAO = DongXeDAO.instance();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testFindById()
    {
        DongXe dx = dongXeDAO.find(10);
        assertEquals(dx.getMa(), "test");
        assertEquals(dx.getTen(), "test junit");
    }
}
