/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.NhanVien;
import Model.Role;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lamvi
 */
public class UserControllerTest {
    static UserController controller;
    public UserControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        controller = UserController.instance();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckLogin() {
        NhanVien nv = new NhanVien();
        nv.setUser_name("admin");
        nv.setPassword("admin");
        nv = controller.checkLogin(nv);
        assertEquals(nv.getMa(), "admin1");
    }
    
    @Test
    public void testGetRole() {
        Role role = controller.getRole(1);
        assertEquals(role.getMa(), "admin");
    }
}
