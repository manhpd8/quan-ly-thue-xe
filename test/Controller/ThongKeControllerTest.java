/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.DoanhThuDongXe;
import Model.DoanhThuXe;
import Model.DoanhThuXeChiTiet;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lamvi
 */
public class ThongKeControllerTest {
    static ThongKeController controller;
    static Date ngay_bd;
    static Date ngay_kt;
    public ThongKeControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        controller = ThongKeController.instance();
        ngay_bd = Date.valueOf("2019-01-01");
        ngay_kt = Date.valueOf("2019-01-21");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testGetDoanhThuDongXe() 
    {
        ArrayList<DoanhThuDongXe> listDTDX = controller.getDoanhThuDongXe(ngay_bd, ngay_kt);
        assertEquals(listDTDX.size(), 1);
        assertEquals(listDTDX.get(0).getTong_doanh_thu(), 15000000);
        assertEquals(listDTDX.get(0).getTong_luot_thue(), 3);
        assertEquals(listDTDX.get(0).getTong_ngay_thue(), 30);
        assertEquals(listDTDX.get(0).getMa(), "test");
        assertEquals(listDTDX.get(0).getTen(), "test junit");
    }
    
    @Test
    public void testGetDoanhThuXeTheoDong()
    {        
        List<DoanhThuXe> listDTX = controller.getDoanhThuXeTheoDong(10, ngay_bd, ngay_kt);
        assertEquals(listDTX.size(), 1);
        assertEquals(listDTX.get(0).getTong_doanh_thu(), 15000000);
        assertEquals(listDTX.get(0).getTong_luot_thue(), 3);
        assertEquals(listDTX.get(0).getTong_ngay_thue(), 30);
        assertEquals(listDTX.get(0).getHang(), "junit");
        assertEquals(listDTX.get(0).getBien(), "99-mm-900000");
    }
    
    @Test
    public void testGetDoanhThuXeChiTiet()
    {
        List<DoanhThuXeChiTiet> listDTXCT = controller.getDoanhThuXeChiTiet(18, ngay_bd, ngay_kt);
        assertEquals(listDTXCT.size(), 3);
        assertEquals(listDTXCT.get(0).getTong_tien(), 9500000);
        assertEquals(listDTXCT.get(1).getTong_tien(), 2500000);
        assertEquals(listDTXCT.get(2).getTong_tien(), 3000000);
    }
}
