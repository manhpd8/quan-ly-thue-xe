/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.sql.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lamvi
 */
public class ThongKeDongXeDoanhThuFrmTest {
    static ThongKeDongXeDoanhThuFrm thongKeDongXeDoanhThuFrm;
    static Date ngay_bd;
    static Date ngay_kt;
    public ThongKeDongXeDoanhThuFrmTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        thongKeDongXeDoanhThuFrm = ThongKeDongXeDoanhThuFrm.instance();
        ngay_bd = Date.valueOf("2019-01-01");
        ngay_kt = Date.valueOf("2019-01-21");
        thongKeDongXeDoanhThuFrm.setjDateChooser_ngayBD(ngay_bd);
        thongKeDongXeDoanhThuFrm.setjDateChooser_ngayKT(ngay_kt);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testGetListDX() 
    {
        thongKeDongXeDoanhThuFrm.btnThongKeClick();
        assertEquals(thongKeDongXeDoanhThuFrm.listDTDX.size(), 1);
        assertEquals(thongKeDongXeDoanhThuFrm.listDTDX.get(0).getMa(), "test");
        assertEquals(thongKeDongXeDoanhThuFrm.listDTDX.get(0).getTen(), "test junit");
        assertEquals(thongKeDongXeDoanhThuFrm.listDTDX.get(0).getTong_luot_thue(), 3);
        assertEquals(thongKeDongXeDoanhThuFrm.listDTDX.get(0).getTong_ngay_thue(), 30);
        assertEquals(thongKeDongXeDoanhThuFrm.listDTDX.get(0).getTong_doanh_thu(), 15000000);
    }
}
