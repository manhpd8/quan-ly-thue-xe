/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Utility.StringUtility;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lamvi
 */
public class LoginFrmTest {
    static LoginFrm loginFrm;
    public LoginFrmTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        loginFrm = new LoginFrm();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testLoginFailse() 
    {
        loginFrm.setUsername("asfawe");
        loginFrm.setPassword("sdfweerw");
        loginFrm.btnLogin();
        assertEquals(loginFrm.getNotify(), StringUtility.LOGIN_FAILSE);
    }
    
    @Test
    public void testLoginPass() 
    {
        loginFrm.setUsername("admin");
        loginFrm.setPassword("admin");
        loginFrm.btnLogin();
        assertEquals(loginFrm.getNhanVien().getMa(), "admin1");
    }
}
