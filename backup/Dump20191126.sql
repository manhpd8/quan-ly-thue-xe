-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cua-hang-thue-xe
-- ------------------------------------------------------
-- Server version	8.0.2-dmr

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `doi_tac`
--

DROP TABLE IF EXISTS `doi_tac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doi_tac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nguoi_id` int(11) NOT NULL,
  `ma` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doi_tac`
--

LOCK TABLES `doi_tac` WRITE;
/*!40000 ALTER TABLE `doi_tac` DISABLE KEYS */;
/*!40000 ALTER TABLE `doi_tac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dong_xe`
--

DROP TABLE IF EXISTS `dong_xe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dong_xe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ma` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ten` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dong_xe`
--

LOCK TABLES `dong_xe` WRITE;
/*!40000 ALTER TABLE `dong_xe` DISABLE KEYS */;
INSERT INTO `dong_xe` VALUES (1,'sedan','Sedan'),(2,'hatchback','Hatchback'),(3,'suv','xe thể thao đa dụng'),(4,'crossover','Crossover'),(5,'mvp','xe đa dụng'),(6,'coupe','xe thể thao'),(7,'convertible ','xe mui trần'),(8,'pickup','xe bán tải'),(9,'limousine','Limousine');
/*!40000 ALTER TABLE `dong_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hoa_don_ky_gui_xe`
--

DROP TABLE IF EXISTS `hoa_don_ky_gui_xe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hoa_don_ky_gui_xe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ngay_bat_dau` datetime NOT NULL,
  `ngay_ket_thuc` datetime NOT NULL,
  `doi_tac_id` int(11) NOT NULL,
  `nhan_vien_id` int(11) NOT NULL,
  `ngay_thanh_toan` datetime DEFAULT NULL,
  `don_gia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoa_don_ky_gui_xe`
--

LOCK TABLES `hoa_don_ky_gui_xe` WRITE;
/*!40000 ALTER TABLE `hoa_don_ky_gui_xe` DISABLE KEYS */;
/*!40000 ALTER TABLE `hoa_don_ky_gui_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hoa_don_thue_xe`
--

DROP TABLE IF EXISTS `hoa_don_thue_xe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hoa_don_thue_xe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ma` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_bat_dau` date NOT NULL,
  `ngay_ket_thuc` date NOT NULL,
  `ngay_thanh_toan` date DEFAULT NULL,
  `dat_coc` int(11) DEFAULT NULL,
  `don_gia` int(11) DEFAULT NULL,
  `tien_phat` int(11) DEFAULT '0',
  `nhan_vien_id` int(11) NOT NULL,
  `khach_hang_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoa_don_thue_xe`
--

LOCK TABLES `hoa_don_thue_xe` WRITE;
/*!40000 ALTER TABLE `hoa_don_thue_xe` DISABLE KEYS */;
INSERT INTO `hoa_don_thue_xe` VALUES (1,'HD01','2019-10-01','2019-10-19','2019-10-19',500000,1000000,500000,1,1),(2,'HD02','2019-10-02','2019-10-15','2019-10-15',500000,1000000,0,1,2),(3,'HD03','2019-10-03','2019-10-16','2019-10-17',500000,1000000,0,1,3),(4,'HD04','2019-11-01','2019-11-11','2019-11-11',NULL,500000,100000,1,1),(5,'HD05','2019-09-02','2019-09-10','2019-09-10',1000000,0,200000,1,4),(6,'HD06','2019-09-15','2019-09-20','2019-09-20',1000000,2000000,300000,1,5),(7,'HD07','2019-09-25','2019-09-30','2019-09-30',1000000,0,500000,1,6),(8,'HD08','2019-10-14','2019-10-20','2019-10-20',1000000,700000,700000,1,7),(9,'HD09','2019-11-11','2019-11-22','2019-11-22',1000000,0,1000000,1,8);
/*!40000 ALTER TABLE `hoa_don_thue_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hop_dong_ky_gui_xe`
--

DROP TABLE IF EXISTS `hop_dong_ky_gui_xe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hop_dong_ky_gui_xe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xe_id` int(11) NOT NULL,
  `hoa_don_ky_gui_xe_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hop_dong_ky_gui_xe`
--

LOCK TABLES `hop_dong_ky_gui_xe` WRITE;
/*!40000 ALTER TABLE `hop_dong_ky_gui_xe` DISABLE KEYS */;
/*!40000 ALTER TABLE `hop_dong_ky_gui_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hop_dong_thue_xe`
--

DROP TABLE IF EXISTS `hop_dong_thue_xe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hop_dong_thue_xe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xe_id` int(11) NOT NULL,
  `hoa_don_thue_xe_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hop_dong_thue_xe`
--

LOCK TABLES `hop_dong_thue_xe` WRITE;
/*!40000 ALTER TABLE `hop_dong_thue_xe` DISABLE KEYS */;
INSERT INTO `hop_dong_thue_xe` VALUES (1,1,1),(2,2,2),(3,7,3),(4,7,4),(5,3,5),(6,4,6),(7,8,7),(8,10,8),(9,14,9);
/*!40000 ALTER TABLE `hop_dong_thue_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khach_hang`
--

DROP TABLE IF EXISTS `khach_hang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `khach_hang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nguoi_id` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ma` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khach_hang`
--

LOCK TABLES `khach_hang` WRITE;
/*!40000 ALTER TABLE `khach_hang` DISABLE KEYS */;
INSERT INTO `khach_hang` VALUES (1,'20','hanoi20',1),(2,'21','hanoi21',1),(3,'22','hanoi22',1),(4,'23','hanoi23',1),(5,'24','hanoi24',1),(6,'25','hanoi25',1),(7,'26','hanoi26',1),(8,'27','hanoi27',1),(9,'28','hanoi28',1),(10,'29','hanoi29',1),(11,'30','hanoi30',1),(12,'31','hanoi31',1);
/*!40000 ALTER TABLE `khach_hang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nguoi`
--

DROP TABLE IF EXISTS `nguoi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nguoi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dien_thoai` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ho` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ten_dem` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ten` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `xa` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `huyen` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tinh` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thanh_pho` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nguoi`
--

LOCK TABLES `nguoi` WRITE;
/*!40000 ALTER TABLE `nguoi` DISABLE KEYS */;
INSERT INTO `nguoi` VALUES (1,'999','admin',NULL,'admin',NULL,NULL,NULL,NULL),(14,'123','nguyễn','văn','nam',NULL,NULL,NULL,NULL),(15,'124','nguyễn','văn','bình',NULL,NULL,NULL,NULL),(16,'126','nguyễn','văn','nhật',NULL,NULL,NULL,NULL),(17,'121','nguyễn','viết','đức',NULL,NULL,NULL,NULL),(18,'111','phạm','đức','cường',NULL,NULL,NULL,NULL),(19,'211','phạm','quý','đức',NULL,NULL,NULL,NULL),(20,'212','tống','thị','ngọc',NULL,NULL,NULL,NULL),(21,'213','vũ','đức','long',NULL,NULL,NULL,NULL),(22,'214','hạng','','vũ',NULL,NULL,NULL,NULL),(23,'215','hạng',NULL,'lương',NULL,NULL,NULL,NULL),(24,'216','lưu',NULL,'bang',NULL,NULL,NULL,NULL),(25,'217','tiêu',NULL,'hà',NULL,NULL,NULL,NULL),(26,'218','trương','','lương',NULL,NULL,NULL,NULL),(27,'219','quảng',NULL,'trọng',NULL,NULL,NULL,NULL),(28,'220','trương',NULL,'bình',NULL,NULL,NULL,NULL),(29,'221','hàn',NULL,'tín',NULL,NULL,NULL,NULL),(30,'222','ngu',NULL,'cơ',NULL,NULL,NULL,NULL),(31,'223','phạm',NULL,'trương',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `nguoi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhan_vien`
--

DROP TABLE IF EXISTS `nhan_vien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nhan_vien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nguoi_id` int(11) NOT NULL,
  `user_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `nam_kinh_nghiem` int(11) DEFAULT NULL,
  `luong` int(11) DEFAULT NULL,
  `ma` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhan_vien`
--

LOCK TABLES `nhan_vien` WRITE;
/*!40000 ALTER TABLE `nhan_vien` DISABLE KEYS */;
INSERT INTO `nhan_vien` VALUES (1,1,'admin','admin',1,1,'admin1',1),(2,14,'nv1','nv1',1,10000000,'staff1',2),(3,15,'nv2','nv2',1,7000000,'staff2',2);
/*!40000 ALTER TABLE `nhan_vien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ma` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ten` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin','quản lý'),(2,'staff','nhân viên');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tai_san`
--

DROP TABLE IF EXISTS `tai_san`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tai_san` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ten` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tinh_trang` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tai_san`
--

LOCK TABLES `tai_san` WRITE;
/*!40000 ALTER TABLE `tai_san` DISABLE KEYS */;
/*!40000 ALTER TABLE `tai_san` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tai_san_hop_dong`
--

DROP TABLE IF EXISTS `tai_san_hop_dong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tai_san_hop_dong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hop_dong_thue_xe_id` int(11) NOT NULL,
  `tai_san_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tai_san_hop_dong`
--

LOCK TABLES `tai_san_hop_dong` WRITE;
/*!40000 ALTER TABLE `tai_san_hop_dong` DISABLE KEYS */;
/*!40000 ALTER TABLE `tai_san_hop_dong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xe`
--

DROP TABLE IF EXISTS `xe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dong_id` int(11) NOT NULL,
  `ma` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ten` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hang` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doi` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mo_ta` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bien` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xe`
--

LOCK TABLES `xe` WRITE;
/*!40000 ALTER TABLE `xe` DISABLE KEYS */;
INSERT INTO `xe` VALUES (1,1,NULL,'honda 1','honda','2015',NULL,'29-r1-1111'),(2,2,NULL,'honda 2','honda','2016',NULL,'29-a2-12345'),(3,3,NULL,'honda 3','honda','2017',NULL,'11-f5-32445'),(4,9,NULL,'limousine 1','limousine','2018',NULL,'33-g6-34455'),(5,9,NULL,'limousine 2','limousine','2019',NULL,'37-k8-54666'),(6,9,NULL,'limousine 3','limousine','2018',NULL,'99-g5-35534'),(7,2,NULL,'limousine 4','limousine','2017',NULL,'66-k0-43566'),(8,4,NULL,'crossover 1','crossover','1995',NULL,'34-6h-435431'),(9,4,NULL,'crossover 2','crossover','1995',NULL,'34-6h-435433'),(10,4,NULL,'crossover 3','crossover','1995',NULL,'34-6h-435432'),(11,5,NULL,'mvp 1','mvp','1995',NULL,'34-6h-34543'),(12,5,NULL,'mvp 3','mvp','1995',NULL,'34-6h-345432'),(13,5,NULL,'mvp 4','mvp','1995',NULL,'34-6h-23325'),(14,8,NULL,'pickup 1','pickup','1996',NULL,'12-fg-435433'),(15,8,NULL,'pickup 2','pickup','1991',NULL,'54-fg-645676'),(16,8,NULL,'pickup 3','pickup','1993',NULL,'67-we-435433'),(17,4,NULL,'crossover 1','crossover','1995',NULL,'34-6h-435433');
/*!40000 ALTER TABLE `xe` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26 23:23:32
