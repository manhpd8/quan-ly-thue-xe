/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.DoanhThuDongXe;
import Model.DoanhThuXe;
import Model.DoanhThuXeChiTiet;
import Model.DongXe;
import connect.DBConnect;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lamvi
 */
public class ThongKeDAO {
    private static ThongKeDAO thongKeDAO;
    public static ThongKeDAO instance(){
        if(thongKeDAO == null){
            thongKeDAO = new ThongKeDAO();
        }
        return thongKeDAO;
    }
    
    private final Connection conn = DBConnect.instance();
    private PreparedStatement ps;
    public ArrayList<DoanhThuDongXe> getDoanhThuDongXe(Date ngay_bd, Date ngay_kt)
    {
        String sql =
            "SELECT * FROM dong_xe as dx left join " +
            "(SELECT dx.id,count(hd.id) as tong_luot_thue, sum(DATEDIFF(hd.ngay_ket_thuc,hd.ngay_bat_dau)) as tong_ngay_thue,sum(hd.don_gia*DATEDIFF(hd.ngay_ket_thuc,hd.ngay_bat_dau) + hd.tien_phat) as tong_doanh_thu " +
            "FROM dong_xe as dx " +
            "LEFT JOIN xe ON xe.dong_id = dx.id " +
            "LEFT JOIN hop_dong_thue_xe as hdtx ON hdtx.xe_id = xe.id " +
            "LEFT JOIN hoa_don_thue_xe as hd ON hd.id = hdtx.hoa_don_thue_xe_id " +
            "WHERE 1=1 AND hd.ngay_bat_dau >= ? AND hd.ngay_ket_thuc <= ? " +
            "GROUP BY dx.id "+
            "ORDER BY tong_doanh_thu desc ) as tbl_tk "+
            "on tbl_tk.id = dx.id";
        ArrayList<DoanhThuDongXe> listDTDX = new ArrayList<>();
        try {
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setDate(1, ngay_bd);
            ps.setDate(2, ngay_kt);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                DoanhThuDongXe dtdx = new DoanhThuDongXe();
                dtdx.setId(rs.getInt("id"));
                dtdx.setMa(rs.getString("ma"));
                dtdx.setTen(rs.getString("ten"));
                dtdx.setTong_ngay_thue(rs.getInt("tong_ngay_thue"));
                dtdx.setTong_luot_thue(rs.getInt("tong_luot_thue"));
                dtdx.setTong_doanh_thu(rs.getLong("tong_doanh_thu"));
                listDTDX.add(dtdx);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listDTDX;
    }
    
    public ArrayList<DoanhThuXe> getDoanhThuXeTheoDong (int dong_id, Date ngay_bd, Date ngay_kt)
    {
        String sql =
            "SELECT xe.id,xe.ma,xe.ten,xe.bien,xe.doi,dx.ten as dong_xe, xe.hang,count(hd.id) as tong_luot_thue, sum(DATEDIFF(hd.ngay_ket_thuc,hd.ngay_bat_dau)) as tong_ngay_thue,sum(hd.don_gia*DATEDIFF(hd.ngay_ket_thuc,hd.ngay_bat_dau) + hd.tien_phat) as tong_doanh_thu " +
            "FROM xe " +
            "JOIN dong_xe as dx ON xe.dong_id = dx.id " +
            "JOIN hop_dong_thue_xe as hdtx ON hdtx.xe_id = xe.id " +
            "JOIN hoa_don_thue_xe as hd ON hd.id = hdtx.hoa_don_thue_xe_id " +
            "WHERE 1=1 AND hd.ngay_bat_dau >= ? AND hd.ngay_ket_thuc <= ? " +
            "AND xe.dong_id = ? " +
            "GROUP BY xe.id " +
            "ORDER BY tong_doanh_thu desc ";
        ArrayList<DoanhThuXe> listDTX = new ArrayList<>();
        DongXe dongXe = DongXeDAO.instance().find(dong_id);
        try {
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setDate(1, ngay_bd);
            ps.setDate(2, ngay_kt);
            ps.setInt(3, dong_id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                DoanhThuXe dtx = new DoanhThuXe();
                dtx.setId(rs.getInt("id"));
                dtx.setMa(rs.getString("ma"));
                dtx.setTen(rs.getString("ten"));
                dtx.setBien(rs.getString("bien"));
                dtx.setDoi(rs.getString("doi"));
                dtx.setHang(rs.getString("hang"));
                dtx.setTong_ngay_thue(rs.getInt("tong_ngay_thue"));
                dtx.setTong_luot_thue(rs.getInt("tong_luot_thue"));
                dtx.setTong_doanh_thu(rs.getLong("tong_doanh_thu"));
                dtx.setDongXe(dongXe);
                listDTX.add(dtx);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listDTX;
    }
    
    public ArrayList<DoanhThuXeChiTiet> getDoanhThuXeChiTiet (int xe_id, Date ngay_bd, Date ngay_kt)
    {
        String sql = "SELECT hdon.*, kh.id as khach_hang_id, hdon.don_gia*DATEDIFF(hdon.ngay_ket_thuc,hdon.ngay_bat_dau)+hdon.tien_phat as tong_tien " +
            "FROM hoa_don_thue_xe as hdon " +
            "JOIN hop_dong_thue_xe as hdong on hdong.hoa_don_thue_xe_id = hdon.id " +
            "JOIN xe on hdong.xe_id = xe.id " +
            "JOIN khach_hang as kh on hdon.khach_hang_id = kh.id " +
            "JOIN nguoi on nguoi.id = kh.nguoi_id " +
            "WHERE hdon.ngay_bat_dau >= ? and hdon.ngay_ket_thuc <= ? " +
            "AND xe.id = ? order by hdon.ngay_bat_dau";
        ArrayList<DoanhThuXeChiTiet> listDTXCT = new ArrayList<>();
        try {
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setDate(1, ngay_bd);
            ps.setDate(2, ngay_kt);
            ps.setInt(3, xe_id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                DoanhThuXeChiTiet dtxct = new DoanhThuXeChiTiet();
                dtxct.setId(rs.getInt("id"));
                dtxct.setMa(rs.getString("ma"));
                int khachHangId = rs.getInt("khach_hang_id");
                dtxct.setKhachHang(UserDAO.instance().findKhacHang(khachHangId));
                dtxct.setNgay_bat_dau(rs.getDate("ngay_bat_dau"));
                dtxct.setNgay_ket_thuc(rs.getDate("ngay_ket_thuc"));
                dtxct.setDon_gia(rs.getInt("don_gia"));
                dtxct.setTien_phat(rs.getInt("tien_phat"));
                dtxct.setTong_tien(rs.getLong("tong_tien"));
                listDTXCT.add(dtxct);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listDTXCT;
    }
}
