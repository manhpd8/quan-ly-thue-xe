/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.KhachHang;
import Model.NhanVien;
import Model.Role;
import connect.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lamvi
 */
public class UserDAO {
    private static UserDAO userDAO;
    public static UserDAO instance(){
        if(userDAO == null){
            userDAO = new UserDAO();
        }
        return userDAO;
    }
    
    private final Connection conn = DBConnect.instance();
    private PreparedStatement ps;
    public NhanVien checkLogin(NhanVien nv){
        String sql = "SELECT * FROM nhan_vien AS nv, nguoi "
                + "WHERE nv.nguoi_id = nguoi.id AND nv.user_name = ? and nv.password = ?";
        try {
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setString(1, nv.getUser_name());
            ps.setString(2, nv.getPassword());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                NhanVien nhanVien = new NhanVien();
                nhanVien.setId(rs.getInt("id"));
                nhanVien.setUser_name(rs.getString("user_name"));
                nhanVien.setPassword(rs.getString("password"));
                nhanVien.setMa(rs.getString("ma"));
                nhanVien.setDient_thoai(rs.getString("dien_thoai"));
                nhanVien.setHo(rs.getString("ho"));
                nhanVien.setTen(rs.getString("ten"));
                nhanVien.setUser_name(rs.getString("user_name"));
                return nhanVien;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public Role getRole(int nvId)
    {
        try {
            String sql = "SELECT r.* " +
                "FROM nhan_vien as nv " +
                "JOIN role as r on nv.role_id = r.id " +
                "WHERE nv.id = ? LIMIT 1";
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setInt(1, nvId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Role role = new Role();
                role.setId(rs.getInt("id"));
                role.setMa(rs.getString("ma"));
                role.setTen(rs.getString("ten"));
                return role;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public KhachHang findKhacHang(int id)
    {
        String sql = "SELECT kh.id, kh.ma, kh.rank, nguoi.ho as ho, nguoi.ten_dem as ten_dem, nguoi.ten as ten " +
            "FROM khach_hang as kh " +
            "JOIN nguoi on kh.nguoi_id = nguoi.id " +
            "where kh.id = ? ";
        try {
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                KhachHang khachHang = new KhachHang();
                khachHang.setId(rs.getInt("id"));
                khachHang.setMa(rs.getString("ma"));
                khachHang.setRank(rs.getInt("rank"));
                khachHang.setHo(rs.getString("ho"));
                khachHang.setTen_dem(rs.getString("ten_dem"));
                khachHang.setTen(rs.getString("ten"));
                return khachHang;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
