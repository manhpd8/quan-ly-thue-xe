/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.DongXe;
import connect.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lamvi
 */
public class DongXeDAO {
    private static DongXeDAO dongXeDAO;
    public static DongXeDAO instance(){
        if(dongXeDAO == null){
            dongXeDAO = new DongXeDAO();
        }
        return dongXeDAO;
    }
    
    private final Connection conn = DBConnect.instance();
    private PreparedStatement ps;
    
    public DongXe find(int id)
    {
        String sql = "SELECT * " +
            "FROM dong_xe " +
            "where id = ? ";
        try {
            ps = (PreparedStatement) conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                DongXe dongXe = new DongXe();
                dongXe.setId(rs.getInt("id"));
                dongXe.setMa(rs.getString("ma"));
                dongXe.setTen(rs.getString("ten"));
                return dongXe;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
