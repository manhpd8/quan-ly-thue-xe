/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lamvi
 */
public class DoanhThuDongXe extends DongXe{
    private int tong_luot_thue;
    private int tong_ngay_thue;
    private long tong_doanh_thu;

    public int getTong_luot_thue() {
        return tong_luot_thue;
    }

    public void setTong_luot_thue(int tong_luot_thue) {
        this.tong_luot_thue = tong_luot_thue;
    }

    public int getTong_ngay_thue() {
        return tong_ngay_thue;
    }

    public void setTong_ngay_thue(int tong_ngay_thue) {
        this.tong_ngay_thue = tong_ngay_thue;
    }

    public long getTong_doanh_thu() {
        return tong_doanh_thu;
    }

    public void setTong_doanh_thu(long tong_doanh_thu) {
        this.tong_doanh_thu = tong_doanh_thu;
    }

    public DoanhThuDongXe() {
    }
    
    public Object[] toObjectDTDX(){
        return new Object[]{id,ma,ten,tong_luot_thue,tong_ngay_thue,tong_doanh_thu};
    }
}
