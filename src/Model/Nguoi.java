/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lamvi
 */
public class Nguoi {
    protected int id;
    protected String dient_thoai;
    protected String ho;
    protected String ten_dem;
    protected String ten;
    protected String xa;
    protected String huyen;
    protected String tinh;
    protected String thanh_pho;

    public Nguoi(int id, String dient_thoai, String ho, String ten_dem, String ten, String thanh_pho) {
        this.id = id;
        this.dient_thoai = dient_thoai;
        this.ho = ho;
        this.ten_dem = ten_dem;
        this.ten = ten;
        this.thanh_pho = thanh_pho;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDient_thoai() {
        return dient_thoai;
    }

    public void setDient_thoai(String dient_thoai) {
        this.dient_thoai = dient_thoai;
    }

    public String getHo() {
        return ho;
    }

    public void setHo(String ho) {
        this.ho = ho;
    }

    public String getTen_dem() {
        return ten_dem;
    }

    public void setTen_dem(String ten_dem) {
        this.ten_dem = ten_dem;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getXa() {
        return xa;
    }

    public void setXa(String xa) {
        this.xa = xa;
    }

    public String getHuyen() {
        return huyen;
    }

    public void setHuyen(String huyen) {
        this.huyen = huyen;
    }

    public String getTinh() {
        return tinh;
    }

    public void setTinh(String tinh) {
        this.tinh = tinh;
    }

    public String getThanh_pho() {
        return thanh_pho;
    }

    public void setThanh_pho(String thanh_pho) {
        this.thanh_pho = thanh_pho;
    }

    public Nguoi() {
    }
}
