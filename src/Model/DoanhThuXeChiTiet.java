/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lamvi
 */
public class DoanhThuXeChiTiet extends HoaDonThueXe{
    
    protected long thanh_tien;
    protected long tong_tien;

    public long getThanh_tien() {
        return thanh_tien;
    }

    public void setThanh_tien(long thanh_tien) {
        this.thanh_tien = thanh_tien;
    }

    public long getTong_tien() {
        return tong_tien;
    }

    public void setTong_tien(long tong_tien) {
        this.tong_tien = tong_tien;
    }
    
    public Object[] toObjectCtDTDXCT(){
        return new Object[]{id,khachHang.ho+" "+khachHang.ten_dem+" "+khachHang.ten,ngay_bat_dau,ngay_ket_thuc,tien_phat,don_gia,tong_tien};
    }
}
