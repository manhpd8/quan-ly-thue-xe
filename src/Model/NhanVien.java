/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lamvi
 */
public class NhanVien extends Nguoi{
    private int id;
    private int nguoi_id;
    private String user_name;
    private String password;
    private int nam_kinh_nghiem;
    private int luong;
    private String ma;
    private int role_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNguoi_id() {
        return nguoi_id;
    }

    public void setNguoi_id(int nguoi_id) {
        this.nguoi_id = nguoi_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNam_kinh_nghiem() {
        return nam_kinh_nghiem;
    }

    public void setNam_kinh_nghiem(int nam_kinh_nghiem) {
        this.nam_kinh_nghiem = nam_kinh_nghiem;
    }

    public int getLuong() {
        return luong;
    }

    public void setLuong(int luong) {
        this.luong = luong;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public NhanVien() {
    }
}
