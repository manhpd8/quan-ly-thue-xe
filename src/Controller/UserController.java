/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.UserDAO;
import Model.NhanVien;
import Model.Role;
/**
 *
 * @author lamvi
 */
public class UserController {
    private static UserController userController;
    public static UserController instance()
    {
        if(userController == null){
            userController = new UserController();
        }
        return userController;
    }
    UserDAO userDAO = UserDAO.instance();
    public NhanVien checkLogin(NhanVien nv)
    {
        return userDAO.checkLogin(nv);
    }
    
    public Role getRole(int nvId)
    {
        return userDAO.getRole(nvId);
    }
}
