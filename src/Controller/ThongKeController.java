/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.ThongKeDAO;
import Model.DoanhThuDongXe;
import Model.DoanhThuXe;
import Model.DoanhThuXeChiTiet;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author lamvi
 */
public class ThongKeController {
    private static ThongKeController thongKeController;
    ThongKeDAO thongKeDAO = ThongKeDAO.instance();
    public static ThongKeController instance()
    {
        if(thongKeController == null){
            thongKeController = new ThongKeController();
        }
        return thongKeController;
    }

    private ThongKeController() {
    }
    
    public ArrayList<DoanhThuDongXe> getDoanhThuDongXe(Date ngay_bd, Date ngay_kt)
    {
        return thongKeDAO.getDoanhThuDongXe(ngay_bd, ngay_kt);
    }
    
    public ArrayList<DoanhThuXe> getDoanhThuXeTheoDong (int dong_id, Date ngay_bd, Date ngay_kt)
    {
        return thongKeDAO.getDoanhThuXeTheoDong(dong_id, ngay_bd, ngay_kt);
    }
    
    public ArrayList<DoanhThuXeChiTiet> getDoanhThuXeChiTiet (int xe_id, Date ngay_bd, Date ngay_kt)
    {
        return thongKeDAO.getDoanhThuXeChiTiet(xe_id, ngay_bd, ngay_kt);
    }
}
